public class Main {
    public static void main(String[] args) {
        MyLinkedList lista = new MyLinkedList();

        lista.addElement(1);
        lista.addElement(2);
        lista.addElement(3);
        lista.addElement(4);
        lista.addElement(5);
        lista.addElement("abrakadabra");

        lista.printList();

        lista.removeLastVersionWithPrevious();
        lista.removeLastVersionWithPrevious();
        lista.removeLastVersionWithPrevious();
        lista.removeLastVersionWithPrevious();
        lista.removeLastVersionWithPrevious();
        lista.removeLastVersionWithPrevious();
//        lista.removeLast();
//        lista.removeLast();
//        lista.removeLast();
//        lista.removeLast();
//        lista.removeLast();
//        lista.removeLast();

        lista.printList();
    }
}
