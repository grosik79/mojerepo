public class MyLinkedList {
    private ListElement root;

    public MyLinkedList() {
        this.root = null;
    }

    public void addElement(Object data) {
        ListElement listElement = new ListElement(data);
        if (root == null) {
            root = listElement;
        } else {
            ListElement tmp = root;
            while (tmp.getNext() != null) {
                tmp = tmp.getNext();
            }

            ListElement toAdd = new ListElement(data);
            tmp.setNext(toAdd);
        }
    }

    public void printList() {
        ListElement tmp = root;
        if(root == null) return;

        System.out.println(tmp.getData());
        while (tmp.getNext() != null) {
            tmp = tmp.getNext();
            System.out.println(tmp.getData());
        }
    }

    public void removeLastVersionWithPrevious() {
        ListElement tmp = root;
        if(root == null) return;

        if (tmp.getNext() != null) {      // wiecej niz 1 element na liscie
            ListElement tmpPrev = root; // przedostatni element

            while (tmp.getNext() != null) {
                tmpPrev = tmp;
                tmp = tmp.getNext();
            }
            tmpPrev.setNext(null);
            // tmp - to ostatni element
        } else {
            root = null;
        }
    }

    public void removeLast() {
        ListElement tmp = root;
        if(root == null) return;

        if (tmp.getNext() != null) {
            while (tmp.getNext() != null && tmp.getNext().getNext() != null) {
                tmp = tmp.getNext();
            }

            tmp.setNext(null);
        } else if(tmp != null && tmp.getNext() == null) { // to znaczy ze jest tylko jeden
                                                    // element root
            root = null;
        }
    }
        public Object get(int indeks){
        if (root == null) throw new ArrayIndexOutOfBoundsException();
        ListElement tmp = root;
        int counter = 0;
        while (tmp.getNext() != null && counter != indeks){
            tmp = tmp.getNext();
            counter++;
                    }
                    if(counter != indeks){
            throw tmp.getData();
        }


}

        public int size(){
    ListElement tmp = root;
    int g = 1;
    while (tmp.getNext() != null){
        g++;
        tmp = tmp.getNext();
    }
    return g;

}
}
